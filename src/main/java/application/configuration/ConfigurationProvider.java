package application.configuration;

import static java.lang.Integer.valueOf;
import static java.lang.System.getenv;
import static java.util.Optional.ofNullable;

public class ConfigurationProvider {

    private static final String REDIS_HOST = "REDIS_HOST";
    private static final String GITLAB_PROJECT_NAME = "GITLAB_PROJECT_NAME";
    private static final String POLL_PERIOD = "POLL_PERIOD";

    private String redisHost;

    private String gitLabProjectName;

    private Integer pollPeriodSeconds;

    public void readEnvironmentVariables(ApplicationConfiguration configuration) {
        redisHost = ofNullable(getenv(REDIS_HOST)).orElse(configuration.getDefaultRedisHost());
        gitLabProjectName = ofNullable(getenv(GITLAB_PROJECT_NAME)).orElse(configuration.getDefaultGitLabProjectName());
        String pollPeriod = getenv(POLL_PERIOD);
        pollPeriodSeconds = pollPeriod != null ? valueOf(pollPeriod) : configuration.getDefaultPollPeriodSeconds();
    }

    public String getRedisHost() {
        return redisHost;
    }

    public void setRedisHost(String redisHost) {
        this.redisHost = redisHost;
    }

    public String getGitLabProjectName() {
        return gitLabProjectName;
    }

    public void setGitLabProjectName(String gitLabProjectName) {
        this.gitLabProjectName = gitLabProjectName;
    }

    public Integer getPollPeriodSeconds() {
        return pollPeriodSeconds;
    }

    public void setPollPeriodSeconds(Integer pollPeriodSeconds) {
        this.pollPeriodSeconds = pollPeriodSeconds;
    }
}
