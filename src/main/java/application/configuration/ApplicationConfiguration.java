package application.configuration;

import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import io.dropwizard.Configuration;

public class ApplicationConfiguration extends Configuration {

    @NotBlank
    private String defaultRedisHost;

    @NotBlank
    private String defaultGitLabProjectName;

    @NotNull
    @Min(1)
    private Integer defaultPollPeriodSeconds;

    public String getDefaultRedisHost() {
        return defaultRedisHost;
    }

    public String getDefaultGitLabProjectName() {
        return defaultGitLabProjectName;
    }

    public Integer getDefaultPollPeriodSeconds() {
        return defaultPollPeriodSeconds;
    }
}
