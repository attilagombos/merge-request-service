package application.module;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Provides;

import javax.ws.rs.client.Client;

import application.client.GitLabMergeRequestApiClient;
import application.client.MergeRequestApiClient;
import application.configuration.ConfigurationProvider;
import application.repository.MergeRequestRepository;
import application.repository.RedisRepository;
import application.resource.HealthCheckResource;
import application.resource.MergeRequestResource;
import application.service.GitLabMergeRequestService;
import application.service.GitLabPollService;
import application.service.MergeRequestService;
import redis.clients.jedis.Jedis;

import static javax.ws.rs.client.ClientBuilder.newClient;

public class ApplicationModule extends AbstractModule {

    private ConfigurationProvider provider;

    public ApplicationModule(ConfigurationProvider provider) {
        this.provider = provider;
    }

    @Override
    protected void configure() {
        bind(MergeRequestApiClient.class).to(GitLabMergeRequestApiClient.class);
        bind(MergeRequestRepository.class).to(RedisRepository.class);
        bind(MergeRequestService.class).to(GitLabMergeRequestService.class);
    }

    @Provides
    public ConfigurationProvider provideConfigurationProvider() {
        return provider;
    }

    @Provides
    public HealthCheckResource provideHealthCheckResource() {
        return new HealthCheckResource();
    }

    @Provides
    public MergeRequestResource provideMergeRequestResource() {
        MergeRequestResource resource = new MergeRequestResource();
        Guice.createInjector(this).injectMembers(resource);
        return resource;
    }

    @Provides
    public GitLabPollService provideGitLabPollService() {
        GitLabPollService service = new GitLabPollService();
        Guice.createInjector(this).injectMembers(service);
        return service;
    }

    @Provides
    public Jedis provideJedis() {
        return new Jedis(provider.getRedisHost());
    }

    @Provides
    public Client provideClient() {
        return newClient();
    }
}
