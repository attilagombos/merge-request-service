package application.client;

import javax.ws.rs.core.Response;

import application.model.MergeRequest;

public interface MergeRequestApiClient {

    Response searchProjects(String name);

    Response getMergeRequests(Long projectId);

    Response updateMergeRequest(MergeRequest request);
}
