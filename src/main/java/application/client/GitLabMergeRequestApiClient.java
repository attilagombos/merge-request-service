package application.client;

import com.google.inject.Inject;

import java.util.function.Function;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import application.model.MergeRequest;

import static javax.ws.rs.client.Entity.entity;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

public class GitLabMergeRequestApiClient implements MergeRequestApiClient {

    private static final Function<Client, WebTarget> TARGET_BASE = client ->
            client.target("https://gitlab.com").path("api/v4/projects");

    @Inject
    private Client client;

    public Response searchProjects(String name) {
        WebTarget target = TARGET_BASE.apply(client)
                .queryParam("search", name);
        return target.request(APPLICATION_JSON_TYPE).get();
    }

    public Response getMergeRequests(Long projectId) {
        WebTarget target = TARGET_BASE.apply(client)
                .path(projectId.toString())
                .path("merge_requests")
                .queryParam("scope", "all");
        return target.request(APPLICATION_JSON_TYPE).get();
    }

    public Response updateMergeRequest(MergeRequest request) {
        WebTarget target = TARGET_BASE.apply(client)
                .path(request.getProjectId().toString())
                .path("merge_requests")
                .path(request.getIid().toString())
                .queryParam("private_token", "Jusu2sk41JyrohtXK_yB"); //Jusu2sk41JyrohtXK_yB
        return target.request(APPLICATION_JSON_TYPE).put(entity(request, APPLICATION_JSON));
    }
}
