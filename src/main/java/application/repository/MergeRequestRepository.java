package application.repository;

import java.util.Set;

public interface MergeRequestRepository {

    void storeRequestId(String id);

    Set<String> retrieveRequestIds();
}
