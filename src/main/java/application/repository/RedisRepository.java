package application.repository;

import com.google.inject.Inject;

import java.util.Set;

import redis.clients.jedis.Jedis;

public class RedisRepository implements MergeRequestRepository {

    private static final String REQUEST_ID_KEY = "merge-request-id";

    @Inject
    private Jedis jedis;

    @Override
    public void storeRequestId(String id) {
        jedis.sadd(REQUEST_ID_KEY, id);
    }

    @Override
    public Set<String> retrieveRequestIds() {
        return jedis.smembers(REQUEST_ID_KEY);
    }
}
