package application.resource;

import com.codahale.metrics.annotation.Timed;
import com.google.inject.Inject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import application.service.GitLabMergeRequestService;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.ok;

@Path("/api/v1")
@Produces(APPLICATION_JSON)
public class MergeRequestResource {

    @Inject
    private GitLabMergeRequestService service;

    @GET()
    @Timed
    @Path("dummy")
    public Response getDummy() {
        return ok("null").build();
    }

    @GET()
    @Timed
    @Path("merge-requests")
    public Response getMergeRequests() {
        return ok(service.getMergeRequestIds()).build();
    }
}
