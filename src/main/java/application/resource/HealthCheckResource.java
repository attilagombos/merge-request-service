package application.resource;

import com.codahale.metrics.annotation.Timed;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import static javax.ws.rs.core.MediaType.APPLICATION_JSON;
import static javax.ws.rs.core.Response.ok;

@Path("/healthcheck")
@Produces(APPLICATION_JSON)
public class HealthCheckResource {

    @GET()
    @Timed
    public Response checkHealth() {
        return ok("ok").build();
    }
}
