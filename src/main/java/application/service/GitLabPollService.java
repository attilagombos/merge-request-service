package application.service;

import com.google.inject.Inject;
import org.slf4j.Logger;

import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import application.configuration.ConfigurationProvider;
import application.model.MergeRequest;
import io.dropwizard.lifecycle.Managed;

import static java.util.concurrent.TimeUnit.SECONDS;
import static org.slf4j.LoggerFactory.getLogger;

public class GitLabPollService implements Managed {

    private static final Logger LOG = getLogger(GitLabPollService.class);

    @Inject
    private ConfigurationProvider provider;

    @Inject
    private GitLabMergeRequestService service;

    private ScheduledExecutorService executorService;

    public GitLabPollService() {
        executorService = Executors.newScheduledThreadPool(1);
    }

    @Override
    public void start() {
        String projectName = provider.getGitLabProjectName();
        Integer pollPeriod = provider.getPollPeriodSeconds();

        service.initializeProject(projectName);

        LOG.info("Starting {} seconds polling of merge requests of GitLab project {}", pollPeriod, projectName);
        executorService.scheduleAtFixedRate(getCommand(), 0, pollPeriod, SECONDS);
    }

    @Override
    public void stop() throws Exception {
        executorService.shutdown();
    }

    private Runnable getCommand() {
        return () -> {
            LOG.info("Starting polling of merge requests of GitLab project {}", provider.getGitLabProjectName());
            List<MergeRequest> mergeRequests = service.getMergeRequests();
            service.updateMergeRequests(mergeRequests);
        };
    }
}
