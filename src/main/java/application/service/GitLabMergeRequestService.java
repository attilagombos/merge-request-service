package application.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.inject.Inject;
import org.slf4j.Logger;

import java.util.List;
import java.util.Set;
import javax.ws.rs.core.Response;

import application.client.GitLabMergeRequestApiClient;
import application.model.MergeRequest;
import application.model.Project;
import application.repository.MergeRequestRepository;

import static java.util.Collections.emptyList;
import static javax.ws.rs.core.Response.Status.Family.SUCCESSFUL;
import static org.slf4j.LoggerFactory.getLogger;

public class GitLabMergeRequestService implements MergeRequestService {

    private static final Logger LOG = getLogger(GitLabMergeRequestService.class);

    private static final ObjectMapper MAPPER = new ObjectMapper();

    @Inject
    private GitLabMergeRequestApiClient gitLabApiClient;

    @Inject
    private MergeRequestRepository repository;

    private Project project;

    @Override
    public void initializeProject(String pathWithNamespace) {
        List<Project> projects = emptyList();

        try {
            String name = pathWithNamespace.split("/")[1];
            Response response = gitLabApiClient.searchProjects(name);
            LOG.info("GitLab response received for searching projects with name {}:\n{}", name, response);

            projects = MAPPER.readValue(response.readEntity(String.class), new TypeReference<List<Project>>(){});
        } catch (Exception e) {
            e.printStackTrace();
        }

        project = projects.stream().filter(p -> pathWithNamespace.equals(p.getPathWithNamespace())).findFirst().orElse(new Project());

        LOG.info("Retrieved ID {} for GitLab project {}", project.getId(), pathWithNamespace);
    }

    @Override
    public List<MergeRequest> getMergeRequests() {
        List<MergeRequest> mergeRequests = emptyList();

        try {
            Response response = gitLabApiClient.getMergeRequests(project.getId());
            LOG.info("GitLab response received for getting merge requests of project with ID {}:\n{}", project.getId(), response);

            mergeRequests = MAPPER.readValue(response.readEntity(String.class), new TypeReference<List<MergeRequest>>(){});
        } catch (Exception e) {
            e.printStackTrace();
        }

        LOG.info("Retrieved merge requests for GitLab project {}", project.getPathWithNamespace());

        return mergeRequests;
    }

    @Override
    public Set<String> getMergeRequestIds() {
        return repository.retrieveRequestIds();
    }

    @Override
    public void updateMergeRequests(List<MergeRequest> mergeRequests) {
        Set<String> requestIds = getMergeRequestIds();
        mergeRequests.stream()
                .filter(mergeRequest -> !requestIds.contains(mergeRequest.getId().toString()))
                .forEach(mergeRequest -> {
                    LOG.info("Found new merge request with ID {} (IID {})", mergeRequest.getId(), mergeRequest.getIid());

                    setSquashCommits(mergeRequest);

                    repository.storeRequestId(mergeRequest.getId().toString());
                    LOG.info("Stored merge request ID {}", mergeRequest.getId());
                });
    }

    private void setSquashCommits(MergeRequest mergeRequest) {
        if (!mergeRequest.getSquash()) {
            mergeRequest.setSquash(true);
            Response response = gitLabApiClient.updateMergeRequest(mergeRequest);
            LOG.info("GitLab response received for setting squash commits for merge request with ID {}:\n{}", mergeRequest.getId(), response);

            if (response.getStatusInfo().getFamily() == SUCCESSFUL) {
                LOG.info("Set squash TRUE for merge request with ID {} (IID {})", mergeRequest.getId(), mergeRequest.getIid());
            } else {
                LOG.error("Failed setting squash to TRUE for merge request with ID {} (IID {}) with reason: {}",
                        mergeRequest.getId(), mergeRequest.getIid(), response.getStatusInfo().getReasonPhrase());
            }
        }
    }
}
