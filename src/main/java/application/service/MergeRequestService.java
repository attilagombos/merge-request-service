package application.service;

import java.util.List;
import java.util.Set;

import application.model.MergeRequest;

public interface MergeRequestService {

    void initializeProject(String pathWithNamespace);

    List<MergeRequest> getMergeRequests();

    Set<String> getMergeRequestIds();

    void updateMergeRequests(List<MergeRequest> mergeRequests);
}
