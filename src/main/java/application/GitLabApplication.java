package application;

import java.util.ArrayList;
import java.util.List;

import application.configuration.ApplicationConfiguration;
import application.configuration.ConfigurationProvider;
import application.module.ApplicationModule;
import application.service.GitLabPollService;
import io.dropwizard.Application;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import static java.util.Arrays.asList;

public class GitLabApplication extends Application<ApplicationConfiguration> {

    public static void main(String[] arguments) throws Exception {
        List<String> serverArguments = new ArrayList<>(asList(arguments));
        serverArguments.add("server");
        serverArguments.add("configuration.yml");
        new GitLabApplication().run(serverArguments.toArray(new String[] {}));
    }


    @Override
    public String getName() {
        return "GitLab App";
    }

    @Override
    public void initialize(Bootstrap<ApplicationConfiguration> bootstrap) {

    }

    @Override
    public void run(ApplicationConfiguration configuration, Environment environment) {
        ConfigurationProvider provider = new ConfigurationProvider();
        provider.readEnvironmentVariables(configuration);

        ApplicationModule module = new ApplicationModule(provider);
        environment.jersey().register(module.provideMergeRequestResource());
        environment.jersey().register(module.provideHealthCheckResource());

        GitLabPollService service = module.provideGitLabPollService();
        service.start();
    }
}
