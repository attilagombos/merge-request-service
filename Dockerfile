FROM gradle:jdk8 as build

COPY --chown=gradle:gradle . /home/gradle/src
WORKDIR /home/gradle/src
RUN gradle clean build

FROM openjdk:8-jre-slim

COPY --from=build /home/gradle/src/build/libs/*-all.jar /app/gitlab-app.jar
COPY --from=build /home/gradle/src/configuration.yml /app/configuration.yml
WORKDIR /app

EXPOSE 43212

ENTRYPOINT ["java", "-jar", "/app/gitlab-app.jar"]

#docker build -f Dockerfile -t app/openjdk:8 .
#docker run -ti --rm -P app/openjdk:8
